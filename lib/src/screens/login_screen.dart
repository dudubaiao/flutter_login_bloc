import 'package:flutter/material.dart';

import '../blocs/login_bloc_provider.dart';
import '../common/ui/forms.dart';
import '../common/ui/layouts.dart';

class LoginScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bloc = LoginBlocProvider.of(context);

    return Container(
      margin: defaultMargin(),
      child: Column(
        children: <Widget>[
          emailField(stream: bloc.email, onChanged: bloc.onEmailChanged),
          passwordField(stream: bloc.password, onChanged: bloc.onPasswordChanged),
          defaultSpace(),
          submitButton(stream: bloc.canSubmit, onSubmit: bloc.submit),
        ],
      ),
    );
  }
}
