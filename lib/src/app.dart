import 'package:flutter/material.dart';

import 'blocs/login_bloc_provider.dart';
import 'screens/login_screen.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Log me in',
      home: Scaffold(
        body: LoginBlocProvider(child: LoginScreen()),
      ),
    );
  }
}
