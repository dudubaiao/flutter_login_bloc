import 'dart:async';

import 'package:rxdart/rxdart.dart';

import 'login_validators.dart';

class LoginBloc {
  final _email = PublishSubject<String>();
  final _password = PublishSubject<String>();

  Function(String) get onEmailChanged => _email.sink.add;

  Stream<String> get email => _email.stream.transform(emailValidator());

  Function(String) get onPasswordChanged => _password.sink.add;

  Stream<String> get password => _password.stream.transform(passwordValidator());

  Stream<LoginState> get canSubmit => Observable.combineLatest2(email, password, _createState);

  LoginState _createState(email, password) => LoginState(email, password);

  submit(LoginState state) {
    print('Email is ' + state.email);
    print('Password is ' + state.password);
  }

  dispose() {
    _email.close();
    _password.close();
  }
}

class LoginState {
  LoginState(this.email, this.password);

  final String email;
  final String password;
}
