import 'dart:async';

StreamTransformer<String, String> emailValidator() => StreamTransformer.fromHandlers(
      handleData: (value, sink) {
        if (value.contains('@')) {
          sink.add(value);
        } else {
          sink.addError('Please enter a valid email');
        }
      },
    );

StreamTransformer<String, String> passwordValidator() => StreamTransformer.fromHandlers(
      handleData: (value, sink) {
        if (value.length >= 4) {
          sink.add(value);
        } else {
          sink.addError('Password must be at least 4 characters long');
        }
      },
    );
