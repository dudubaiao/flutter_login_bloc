import 'package:flutter/material.dart';

EdgeInsets defaultMargin() => EdgeInsets.all(20.0);

Widget defaultSpace() => SizedBox(height: 10.0);
