import 'package:flutter/material.dart';

Widget emailField({@required Stream<String> stream, @required ValueChanged<String> onChanged}) {
  return StreamBuilder(
    stream: stream,
    builder: (BuildContext context, AsyncSnapshot snapshot) {
      //
      return TextField(
        decoration: InputDecoration(
            labelText: 'Email address',
            hintText: 'yourname@example.com',
            errorText: snapshot.error),
        keyboardType: TextInputType.emailAddress,
        onChanged: onChanged,
      );
    },
  );
}

Widget passwordField({@required Stream<String> stream, @required ValueChanged<String> onChanged}) {
  return StreamBuilder(
      stream: stream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        //
        return TextField(
          decoration: InputDecoration(
            labelText: 'Password',
            hintText: 'your password here',
            errorText: snapshot.error,
          ),
          obscureText: true,
          onChanged: onChanged,
        );
      });
}

Widget submitButton({@required Stream<dynamic> stream, @required Function onSubmit}) {
  return StreamBuilder(
      stream: stream,
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        //
        return RaisedButton(
          child: Text('Submit'),
          color: Colors.blue,
          onPressed: snapshot.hasData
              ? () {
                  onSubmit(snapshot.data);
                }
              : null,
        );
      });
}
